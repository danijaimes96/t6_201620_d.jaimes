package Tests;

import taller.estructuras.TablaHash;
import junit.framework.TestCase;

public class Test extends TestCase
	{
		private TablaHash<String, String> tabla;

		public void setUp1()
			{
				tabla = new TablaHash<>(10,10);
			}

		public void setUp2()
			{
				tabla = new TablaHash<>(10,10);	
				tabla.put("dani", "Yes");
				tabla.put("santi", "Sssi");
				tabla.put("sebas", "peque");
				tabla.put("maria", "oui");
			}
		public void testPut()
			{
				setUp1();
				tabla.put("dani", "Yes");
				tabla.put("santi", "Sssi");
				tabla.put("sebas", "peque");
				tabla.put("maria", "oui");
				assertEquals("Yes", tabla.get("dani"));
				tabla.put("dani", "cambio");
				assertEquals("cambio", tabla.get("dani"));
			}
		public void testget()
			{
				setUp2();
				
				assertEquals("Yes", tabla.get("dani"));
				assertNotSame("Yes", tabla.get("sebas"));
				
			}
		
		public void testDelete()
			{
				setUp2();
				tabla.delete("sebas");
				assertEquals(null,  tabla.get("sebas"));
			}
		
	}
