package Tests;

import java.util.LinkedList;

import junit.framework.TestCase;
import taller.estructuras.TablaHashDos;

public class TestDos extends TestCase
{
	private TablaHashDos<String, String> tabla;

	public void setUp1()
		{
			tabla = new TablaHashDos<String, String>();
		}

	public void setUp2()
		{
			tabla = new TablaHashDos<String, String>();	
			tabla.put("dani", "Yes");
			tabla.put("santi", "Sssi");
			tabla.put("sebas", "peque");
			tabla.put("maria", "oui");
		}
	public void testPut()
		{
			setUp1();
			tabla.put("dani", "Yes");
			tabla.put("santi", "Sssi");
			tabla.put("sebas", "peque");
			tabla.put("maria", "oui");
			LinkedList<String> resp= new LinkedList<>();
			resp.add("Yes");
			assertEquals(resp, tabla.get("dani"));
			tabla.put("dani", "cambio");
			resp.add("cambio");
			assertEquals(resp, tabla.get("dani"));
		}
	public void testget()
		{
			setUp2();
			LinkedList<String> resp= new LinkedList<>();
			resp.add("Yes");
			assertEquals(resp, tabla.get("dani"));
			assertNotSame("Yes", tabla.get("sebas"));
			
		}
	
	public void testDelete()
		{
			setUp2();
			tabla.delete("sebas");
			assertEquals(null,  tabla.get("sebas"));
		}
	

}
