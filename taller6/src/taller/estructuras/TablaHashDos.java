package taller.estructuras;

import java.util.LinkedList;

public class TablaHashDos<K extends Comparable<K>, V>
{
	private TablaHash<K, LinkedList<V>> tabla;

	public TablaHashDos()
	{
		tabla = new TablaHash<K, LinkedList<V>>();

	}

	public void put(K llave, V valor)
	{
		LinkedList<V> listita = tabla.get(llave);
		if (listita == null)
		{
			listita = new LinkedList<>();
		}
		listita.add(valor);
		tabla.put(llave, listita);
	}

	public LinkedList<V> get(K llave)
	{
		return tabla.get(llave);
	}

	public void delete(K llave)
	{
		tabla.delete(llave);
	}
}
