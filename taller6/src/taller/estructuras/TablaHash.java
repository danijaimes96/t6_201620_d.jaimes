package taller.estructuras;

import java.util.LinkedList;

@SuppressWarnings("unchecked")
public class TablaHash<K extends Comparable<K>, V>
{

	// TODO Una enumeracioón que representa los tipos de colisiones que se
	// pueden manejar
	public static enum Colisiones
	{
		LINEAR_PROBING, SEPARATE_CHAINING
	}

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	// TODO: Agruegue la estructura que va a soportar la tabla.
	private LinkedList<NodoHash<K, V>>[] tablita;
	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	private static final int TAMANO = 125000;

	// Constructores

	public TablaHash()
	{
		capacidad = TAMANO;
		tablita = new LinkedList[TAMANO];
		factorCargaMax = 8f;
		// TODO: Inicialice la tabla con los valores que considere
		// prudentes
		// para una ejecución normal
	}

	public TablaHash(int capacidad, float factorCargaMax)
	{
		// TODO: Inicialice la tabla con los valores dados por parametro

		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		tablita = new LinkedList[1024];

	}

	public void put(K llave, V valor)
	{
		// TODO: Gaurde el objeto valor dado por parametro el cual tiene
		// la
		// llave,
		// tenga en cuenta que puede o no puede haber colisiones
		int index = hash(llave);
		// System.out.println("cosito put" + tablita.length);

		if (tablita[index] != null)
		{
			for (NodoHash<K, V> nodo : tablita[index])
			{
				if (nodo.getLlave().equals(llave))
				{
					nodo.setValor(valor);
					return;
				}
			}
		}
		else
		{
			tablita[index] = new LinkedList<NodoHash<K, V>>();
		}// :(

		tablita[index].add(new NodoHash<K, V>(llave, valor));
		count++;
		calcFactorCarga();
		if (factorCarga >= factorCargaMax)
		{
//			System.err.println("REHASH");
			rehash();
		}
	}

	public V get(K llave)
	{
		// TODO
		// parametro.
		// Tenga en cuenta
		// colisiones
		int index = hash(llave);
		V buscado = null;
		if (tablita[index] != null)
		{
			for (NodoHash<K, V> n : tablita[index])
			{
				if (n.getLlave().equals(llave))
				{
					buscado = n.getValor();
					break;
				}

			}
		}

		return buscado;
	}

	public V delete(K llave)
	{
		// TODO: borra el objeto cuya llave es la dada por parametro.
		// Tenga en
		// cuenta
		// colisiones
		int index = hash(llave);
		V buscado = null;
		NodoHash<K, V> del = null;
		if (tablita[index] != null)
		{

			for (NodoHash<K, V> n : tablita[index])
			{
				if (n.getLlave().equals(llave))
				{
					del = n;
					buscado = n.getValor();
					break;
				}
			}

			tablita[index].remove(del);
			count--;
			calcFactorCarga();
			// if(factorCarga >= factorCargaMax)
			// {
			// rehash("down");
			// }
		}
		return buscado;

	}

	// Hash
	private int hash(K llave)
	{
		// TODO: Escriba una función de Hash, recuerde tener en cuenta
		// la
		// complejidad de ésta así como las colisiones.
		int num = llave.hashCode();
		int numReal = num % tablita.length;
		numReal = Math.abs(numReal);
		return numReal;
	}

	// TODO: Permita que la tabla sea dinamica
	private void rehash()
	{

		// System.arraycopy(tablita, 0, temp, 0, temp.length);
		LinkedList<NodoHash<K, V>> temp = new LinkedList<>();
		// tablita = new LinkedList[capacidad * 3];
		for (LinkedList<NodoHash<K, V>> linkedList : tablita)
		{
			if (linkedList != null)
			{
				temp.addAll(linkedList);
			}
		}
		capacidad += TAMANO;
		tablita = new LinkedList[capacidad];
		count = 0;
		for (NodoHash<K, V> n : temp)
		{
			put(n.getLlave(), n.getValor());
		}
	}

	private void calcFactorCarga()
	{
		factorCarga = count / capacidad;
	}

	//

	public String toString()
	{
		String respuesta = "";
		for (int i = 0; i < tablita.length; i++)
		{
			if (tablita[i] != null)
			{
				respuesta += tablita[i].toString();
			}
		}
		return respuesta;
	}

	// public String toString( )
	// {
	// StringBuilder sBuilder = new StringBuilder( );
	//
	// int cols = 1;
	// for( LinkedList<NodoHash<K, V>> linkedList : tablita )
	// {
	// if( linkedList != null )
	// {
	// cols = Math.max( linkedList.size( ), cols );
	// }
	// }
	//
	// int posArreglo = 0;
	// DecimalFormat format = new DecimalFormat( "000" );
	// for( int j = 0; j < 4; j++ )
	// {
	// if( j == 0 )
	// {
	// sBuilder.append( "_____" );
	// }
	// else if( j == 3 )
	// {
	// sBuilder.append( "││_____││" );
	// }
	// else if( j == 2 )
	// {
	// sBuilder.append( "││   POS   ││" );
	// }
	// else
	// {
	// sBuilder.append( "││         ││" );
	// }
	// for( int i = 0; i < cols; i++ )
	// {
	// if( j == 0 )
	// {
	// sBuilder.append( "________" );
	// }
	// else if( j == 3 )
	// {
	// sBuilder.append( "______││" );
	// }
	// else if( j == 2 )
	// {
	// sBuilder.append( "      " + ( i + 1 ) + " COL     ││" );
	// }
	// else
	// {
	// sBuilder.append( "                ││" );
	// }
	// }
	// sBuilder.append( "\n" );
	// }
	// int line = 16;
	// for( LinkedList<NodoHash<K, V>> linkedList : tablita )
	// {
	// sBuilder.append( "││   " + format.format( posArreglo++ ) + "   ││ "
	// );
	// if( linkedList != null )
	// {
	// for( NodoHash<K, V> nodoHash : linkedList )
	// {
	// String data = nodoHash.toString( );
	// if( data.length( ) < line )
	// {
	// int cant = ( line / 2 ) - 1 - ( data.length( ) / 2 );
	// String space = "               ".substring( 0, cant );
	// String def = space + data + space;
	// def += "                     ".substring( 0, line - 1 - def.length( )
	// );
	// sBuilder.append( def + "││ " );
	// }
	// else
	// {
	// sBuilder.append( data.substring( 0, line - 5 ) + "... ││ " );
	// }
	// }
	// for( int i = linkedList.size( ); i < cols; i++ )
	// {
	// sBuilder.append( "               ││ " );
	// }
	// }
	// else
	// {
	// for( int i = 0; i < cols; i++ )
	// {
	// sBuilder.append( "               ││ " );
	// }
	// }
	// sBuilder.append( "\n" );
	// }
	//
	// sBuilder.append( "┴┴─────────┴┴" );
	// for( int i = 0; i < cols; i++ )
	// {
	// sBuilder.append( "────────────────┴┴" );
	//
	// }
	// sBuilder.append( "\n" );
	//
	// return sBuilder.toString( );
	// }
	// public static void main( String[ ] args )
	// {
	// TablaHash<String, Integer> hash = new TablaHash<>( );
	//
	// String[ ] abc = new String[ ]
	// {
	// "A",
	// "B",
	// "C",
	// "D",
	// "E",
	// "F",
	// "G",
	// "H",
	// "I",
	// "J",
	// "K",
	// "L",
	// "M",
	// "N",
	// "O",
	// "P",
	// "Q",
	// "R",
	// "S",
	// "T",
	// "U",
	// "V",
	// "W",
	// "X",
	// "Y",
	// "Z"
	// };
	//
	// Random r = new Random( );
	//
	// for( int i = 0; i < 41; i++ )
	// {
	// String val = "";
	// val += abc[ r.nextInt( abc.length ) ];
	// val += abc[ r.nextInt( abc.length ) ];
	// val += abc[ r.nextInt( abc.length ) ];
	// val += abc[ r.nextInt( abc.length ) ];
	// val += abc[ r.nextInt( abc.length ) ];
	// hash.put( val, i );
	// }
	//
	//
	//
	// System.out.println( hash.toString( ) );
	//
	// }
}