package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedList;

import mundo.Habitante;
import taller.estructuras.TablaHash;
import taller.estructuras.TablaHashDos;

public class Main
{

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static TablaHash<Integer, Habitante> tablaHabitantes;
	private static TablaHashDos<String, Integer> tablaNom;
	private static TablaHashDos<Integer, Integer> tablaLoc;

	public static void main(String[] args)
	{
		// Cargar registros
		System.out
				.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out
				.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(
					rutaEntrada)));
			// String entrada = br.readLine();

			// TODO: Inicialice el directorio t
			tablaHabitantes = new TablaHash<>();
			tablaNom = new TablaHashDos<>();
			tablaLoc = new TablaHashDos<Integer, Integer>();
			int i = 0;
			String entrada = br.readLine();
			long ptiempo = new Date().getTime();
			while (entrada != null)
			{
				String[] datos = entrada.split(";");
				Habitante nueva = new Habitante(datos[0], datos[1],
						Integer.parseInt(datos[2]), Integer.parseInt(datos[3]),
						Integer.parseInt(datos[4]));

				// System.out.println(datos[0]+
				// datos[1]+Integer.parseInt(datos[2])+
				// Integer.parseInt(datos[3])+Integer.parseInt(datos[4]));
				tablaHabitantes.put(nueva.getNumero(), nueva);
				tablaNom.put(nueva.getNombre(), nueva.getNumero());
				tablaLoc.put(nueva.getLat(), nueva.getNumero());
				// TODO: Agrege los datos al directorio de
				// emergencias
				// Recuerde revisar en el enunciado la
				// estructura de la información
				++i;
				if (++i % 500000 == 0)
				{
					long dtiempo = new Date().getTime();
					long time = dtiempo - ptiempo;
					System.out.println(i + " entradas...");
					System.out.println("Le tomo: " + time);
				}
				entrada = br.readLine();
			}
			System.out.println(i + " entradas cargadas en total");
			br.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out
				.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try
			{
				System.out
						.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out
						.println("1: Agregar ciudadano a la lista de emergencia");
				System.out
						.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out
						.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(
						System.in));
				String in = br.readLine();
				switch (in)
				{
					case "1":
						// TODO: Implemente el requerimiento 1.
						// Agregar ciudadano a la lista de
						// emergencia
						System.out
								.println("escriba el nombre del ciudadano. \n");
						String nombre = br.readLine();
						System.out
								.println("escriba el apellido del ciudadano. \n");
						String apellido = br.readLine();
						System.out
								.println("escriba el telefono del ciudadano. \n");
						int tel = Integer.parseInt(br.readLine());
						System.out
								.println("escriba la latitud del ciudadano. \n");
						int lat = Integer.parseInt(br.readLine());
						System.out
								.println("escriba lo longitud del ciudadano. \n");
						int longt = Integer.parseInt(br.readLine());
						tablaHabitantes.put(tel, new Habitante(nombre,
								apellido, tel, lat, longt));
						tablaNom.put(nombre, tel);
						tablaLoc.put(lat, tel);//:D
						break;
					case "2":
						// TODO: Implemente el requerimiento 2
						// Buscar un ciudadano por el número de
						// teléfono de su acudiente
						System.out
								.println("escriba el telefono del ciudadano. \n");
						int tel1 = Integer.parseInt(br.readLine());

						Habitante resp = tablaHabitantes.get(tel1);
						if (resp == null)
						{
							System.out.println("No existe ese numero");
						}
						else
						{
							System.out.println(resp.toString());
						}
						break;
					case "3":
						// TODO: Implemente el requerimiento 3
						// Buscar ciudadano por apellido/nombre
						System.out
								.println("escriba el nombre del ciudadano. \n");
						String nomb = br.readLine();

						LinkedList<Integer> resp2 = tablaNom.get(nomb);

						if (resp2 == null)
						{
							System.out.println("No existe ese nombre");
						}
						else
						{
							for (Integer integer : resp2)
							{
								Habitante resp3 = tablaHabitantes.get(integer);
								System.out.println(resp3);
							}
						}

						break;
					case "4":
						// TODO: Implemente el requerimiento 4
						// Buscar ciudadano por su locaclización
						// actual
						System.out
								.println("escriba la lat del  del ciudadano. \n");
						Integer lat2 = Integer.parseInt(br.readLine());

						LinkedList<Integer> resp4 = tablaLoc.get(lat2);

						if (resp4 == null)
						{
							System.out.println("la Latitud  no existe ");
						}
						else
						{
							for (Integer integer : resp4)
							{
								Habitante resp5 = tablaHabitantes.get(integer);
								System.out.println(resp5);
							}
						}
						break;
					case "Exit":
						System.out.println("Cerrando directorio...");
						seguir = false;
						break;

					default:
						break;
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

	}

}
