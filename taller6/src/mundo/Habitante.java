package mundo;

public class Habitante
	{
		private String nombre;
		private String apellido;
		private int numero;
		private int lat;
		private int longt;

		public Habitante(String nombre, String apellido, int numero, int lat, int longt)
			{
				super();
				this.nombre = nombre;
				this.apellido = apellido;
				this.numero = numero;
				this.lat=lat;
				this.longt= longt;
			}


		public int getLat()
			{
				return lat;
			}

		public void setLat(int lat)
			{
				this.lat = lat;
			}

		public int getLongt()
			{
				return longt;
			}

		public void setLongt(int longt)
			{
				this.longt = longt;
			}

		public String getNombre()
			{
				return nombre;
			}

		public void setNombre(String nombre)
			{
				this.nombre = nombre;
			}

		public String getApellido()
			{
				return apellido;
			}

		public void setApellido(String apellido)
			{
				this.apellido = apellido;
			}

		public int getNumero()
			{
				return numero;
			}

		public void setNumero(int numero)
			{
				this.numero = numero;
			}

		public String toString()
			{
				String resp = nombre + "\n" + apellido + "\n" + numero+ "\n" + lat + "\n" + longt;
				return resp;
			}

	}
